<?php

function theme_styles()
{
    wp_enqueue_style('bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
    wp_enqueue_style('sidebar_css', get_stylesheet_directory_uri() . '/css/sidebar.css');
    wp_enqueue_style('main_css', get_stylesheet_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'theme_styles');

function theme_js()
{
    global $wp_scripts;
    wp_enqueue_script('jquery_js', 'https://code.jquery.com/jquery-3.3.1.slim.min.js');
    wp_enqueue_script('popper_js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js');
    wp_enqueue_script('bootstrap_js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js');
    wp_enqueue_script('main_js', get_stylesheet_directory_uri() . '/js/main.js');
}

add_action('wp_enqueue_scripts', 'theme_js');

function custom_excerpt_length( $length ) {
    return 150;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function pickmania_setup()
{
    add_theme_support('post-thumbnails');
    add_image_size('sliders', 1300, 800, true);
    add_image_size('post_detail', 1300, 600, true);
    add_image_size('matches', 400, 250, true);
}
add_action('after_setup_theme', 'pickmania_setup');

register_nav_menus(array(
    'menu_principal' => __('Menu Principal', 'PlantillaWP'),
));

// AGREGAR ASYNC Y DEFERS
function agregar_async_defer($tag, $handle){
    if('maps' !== $handle)
        return $tag;
    return str_replace('src', 'async="async" defer="defer" src', $tag);
}
add_filter('script_loader_tag', 'agregar_async_defer', 10, 2);

// creacion de menus
function pickmania_menus()
{
    register_nav_menus(array(
        'header-menu' => __('Header Menu', 'pickmania'),
        'social-menu' => __('Social Menu', 'pickmania'),
    ));
}
add_action('init', 'pickmania_menus');

// CUSTON POST TYPE
add_action('init', 'pickmania_free_picks');
function pickmania_free_picks()
{
    $labels = array(
        'name'               => _x('Free Picks', 'pickmania'),
        'singular_name'      => _x('Free Picks', 'post type singular name', 'pickmania'),
        'menu_name'          => _x('Free Picks', 'admin menu', 'pickmania'),
        'name_admin_bar'     => _x('Free Picks', 'add new on admin bar', 'pickmania'),
        'add_new'            => _x('Add New', 'book', 'pickmania'),
        'add_new_item'       => __('Add New Pick', 'pickmania'),
        'new_item'           => __('New Pick', 'pickmania'),
        'edit_item'          => __('Edit Pick', 'pickmania'),
        'view_item'          => __('View Pick', 'pickmania'),
        'all_items'          => __('All Pick', 'pickmania'),
        'search_items'       => __('Search Pick', 'pickmania'),
        'parent_item_colon'  => __('Parent Pick:', 'pickmania'),
        'not_found'          => __('No Pick found.', 'pickmania'),
        'not_found_in_trash' => __('No Pick found in Trash.', 'pickmania'),
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __('Description.', 'pickmania'),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'free_picks'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 6,
        // 'taxonomies'         => array( 'category' ),
        'supports'           => array('title', 'editor', 'thumbnail', 'author', 'excerpt', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'post-formats'),
    );
    register_post_type('free_picks', $args);
}

// CUSTON POST TYPE
add_action('init', 'pickmania_expert_picks');
function pickmania_expert_picks()
{
    $labels = array(
        'name'               => _x('Expert Picks', 'pickmania'),
        'singular_name'      => _x('Expert Picks', 'post type singular name', 'pickmania'),
        'menu_name'          => _x('Expert Picks', 'admin menu', 'pickmania'),
        'name_admin_bar'     => _x('Expert Picks', 'add new on admin bar', 'pickmania'),
        'add_new'            => _x('Add New', 'book', 'pickmania'),
        'add_new_item'       => __('Add New Pick', 'pickmania'),
        'new_item'           => __('New Pick', 'pickmania'),
        'edit_item'          => __('Edit Pick', 'pickmania'),
        'view_item'          => __('View Pick', 'pickmania'),
        'all_items'          => __('All Pick', 'pickmania'),
        'search_items'       => __('Search Pick', 'pickmania'),
        'parent_item_colon'  => __('Parent Pick:', 'pickmania'),
        'not_found'          => __('No Pick found.', 'pickmania'),
        'not_found_in_trash' => __('No Pick found in Trash.', 'pickmania'),
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __('Description.', 'pickmania'),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'expert_picks'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 6,
        // 'taxonomies'         => array( 'category' ),
        'supports'           => array('title', 'editor', 'thumbnail', 'author', 'excerpt', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'post-formats'),
    );
    register_post_type('expert_picks', $args);
}



// PAGINATION
function paginationPickmania() {
    global $wp_query;
 
    if ( $wp_query->max_num_pages > 1 ) { ?>
        <nav class="pagination" role="navigation">
            <div class="nav-previous"><?php next_posts_link( '&larr; Anterior',true ); ?></div>
            <div class="nav-next"><?php previous_posts_link( 'Siguiente &rarr;' ,true); ?></div>
        </nav>
<?php }
 wp_reset_postdata();
}