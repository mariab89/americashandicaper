<footer>
    <div class="container-fluid footer1 pl-5">
        <div class="row">
            <div class="col-md-3">
                <h4 class="text-white">INFORMATION</h4>
                <ul class="listFooter pl-0">
                    <li>
                        <a href="" class="text-white">
                            BANKING
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            BONUS RULES
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            PRIVACY POLICY
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            TERMS & CONDITIONS
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4 class="text-white">CONTACT</h4>
                <ul class="listFooter pl-0">
                    <li>
                        <a href="" class="text-white">
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            CONTACT
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            ABOUT US
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            RESPONSIBLE GAMING
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="row providerPays">
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/1.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/2.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/3.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/4.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/8.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/5.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/6.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/7.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/9.png" width="100%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/10.png" width="100%" class="hvr-pulse-shrink">
            </div>
        </div>
    </div>
</footer>



<?php wp_footer()?>

</body>

</html>