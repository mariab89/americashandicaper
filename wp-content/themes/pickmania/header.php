
<!DOCTYPE html>
<html lang="<?php language_attributes();?>">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo('pingback_url')?>">
	<title><?php wp_title('')?> <?php if (wp_title('', false)) {echo ' : ';}?> <?php bloginfo('name')?></title>
	<!-- IOS -->
	<meta name="apple-mobil-web-app-capable" content="yes">
	<meta name="apple-mobil-web-app-title" content="PREGAMES NOW">
	<link rel="apple-touch-icon" href="<?= get_template_directory_uri() ?>/img/logo.png" >

	<!-- ANDROID -->
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="#262E69">
	<meta name="aplication-name" content="PREGAMES NOW">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri() ?>/img/logo.png" sizes="192x192">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	<link href="<?= get_template_directory_uri() ?>/css/hover-min.css" rel="stylesheet" media="all">
	<?php wp_head();?>
</head>
<body <?php body_class();?>>

<header>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="hvr-float-shadow">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adds/1.gif" alt="Banners Add" width="100%" height="auto" />
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbarHeader">
                    <a class="navbar-brand animated infinite pulse delay-2s hvr-float-shadow" href="<?= esc_url(home_url('/')) ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" width="250px">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><i class="fas fa-bars text-white"></i></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item mr-2 <?php if (is_page('matches')) echo 'active'; ?>">
                                <a class="nav-link hvr-float-shadow" href="<?= esc_url(home_url('/matches')) ?>"">IMPORTANT MATCHES</a>
                            </li>
                            <li class="nav-item mr-2 <?php if (is_page('bookmakers')) echo 'active'; ?>">
                                <a class="nav-link hvr-float-shadow" href="<?= esc_url(home_url('/bookmakers')) ?>">TOP BOOKMAKERS</a>
                            </li>
                            <li class="nav-item mr-2 <?php if (is_page('bookmarkers')) echo 'active'; ?>">
                                <a class="nav-link hvr-float-shadow" href="#">NEWS</a>
                            </li>
                            <li class="nav-item mr-2 <?php if (is_page('free-picks') || is_page('expert-picks')) echo 'active'; ?> dropdown">
                                <a class="nav-link dropdown-toggle hvr-float-shadow" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    PICKS
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item <?php if (is_page('free-picks')) echo 'active'; ?>" href="<?= esc_url(home_url('/free-picks')) ?>">FREE PICKS</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item <?php if (is_page('expert-picks')) echo 'active'; ?>" href="<?= esc_url(home_url('/expert-picks')) ?>">EXPERT PICKS</a>
                                </div>
                            </li>
                            <li class="nav-item mr-2 <?php if (is_page('bookmarkers')) echo 'active'; ?> dropdown">
                                <a class="nav-link dropdown-toggle hvr-float-shadow" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                SPORTS
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php
                                        $categories = get_categories();
                                        foreach($categories as $category) {
                                    ?>
                                        <a class="dropdown-item" href="<?= get_category_link($category->term_id) ?>"><?= $category->cat_name ?></a>
                                        <div class="dropdown-divider"></div>
                                    <?php } ?>
                                </div>
                            </li>
                            <li class="nav-item mr-2 <?php if (is_page('bookmarkers')) echo 'active'; ?> dropdown">
                                <a class="nav-link dropdown-toggle hvr-float-shadow" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    EXTRA
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">HOROSCOPES</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">CELEBRITY & HISTORY</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">LOTTERY</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">WEATHER</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">GAS</a>
                                </div>
                            </li>
                            <li class="nav-item mr-2 <?php if (is_page('bookmarkers')) echo 'active'; ?>">
                                <a class="nav-link hvr-float-shadow" href="#">REGISTER</a>
                            </li>
                            <li class="nav-item mr-2 <?php if (is_page('bookmarkers')) echo 'active'; ?>">
                                <a class="nav-link hvr-float-shadow" href="#">LOGIN</a>
                            </li>
                            <li>
                                <form class="form-inline">
                                    <div class="input-group float-right">
                                        <input type="text" class="form-control inputSearch" aria-describedby="search">
                                        <div class="input-group-prepend">
                                            <span id="search">
                                                <i class="fas fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>