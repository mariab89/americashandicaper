<section class="sidebar row p-2 mt-3">
    <!-- FORMULARIO -->
    <div class="col-md-12 formSendPick mt-3 text-center">
        <img src="<?= get_template_directory_uri() ?>/img/free-picks.png" width="100%" alt="">
        <form class="text-center p-4">
            <div class="form-group">
                <input type="text" class="form-control inputFormSendPick" name="name" id="nameSend" placeholder="Name" />
            </div>
            <div class="form-group">
                <input type="email" class="form-control inputFormSendPick" name="email" id="emailSend" placeholder="Email Address" />
            </div>
            <button type="submit" class="btn btn-sendFormPick btn-block">SEND ME FREE PICKS!</button>
        </form>
    </div>
    <div class="col-md-12 text-center text-white">
        banner
    </div>
    <div class="col-md-12 text-center text-white">
        banner
    </div>
    <div class="col-md-12 text-center text-white">
        banner
    </div>

    <?php if (!is_page('bookmakers')){ ?> 
        <div class="col-md-12 table-responsive mb-5">
            <table class="table tableSidebar table-hover table-bordered">
                <caption class="p-2">
                    <h4 class="text-white">Title</h4>
                </caption>
                <tbody>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #1
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <a href="#">
                                Title
                            </a>
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #2
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <a href="#">
                                Title
                            </a>
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #3
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <a href="#">
                                Title
                            </a>
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #4
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <a href="#">
                                Title
                            </a>
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #5
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <a href="#">
                                Title
                            </a>
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    <?php } ?>
</section>