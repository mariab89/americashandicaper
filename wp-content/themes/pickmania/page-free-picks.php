<?php get_header();?>

<section class="container-fluid mb-5">
    <div class="row">
        <div class="col-md-12 col-xs-12 pl-0 pr-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/slider1.jpg" height="800px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid bookmakers mt-5 mb-5 p-5">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="row mr-2">
                <div class="col-md-12 matchesItems pl-5 pr-5">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <h4 class="bg-light text-center p-3 font-weight-bold">
                                FREE PICKS
                            </h4>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <p class="float-right">Imga</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <?php 
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                            $args = array(
                                'post_type' => 'free_picks',
                                'posts_per_page' => 10,
                                'order' => 'DESC',	
                                'paged' => $paged
                            ); 
                        ?>
                        <?php $posts = new WP_Query($args);  ?>
                        <?php while($posts->have_posts() ){ $posts->the_post();?>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12">
                                        <a href="<?php the_permalink(); ?>" class="hvr-float-shadow">
                                            <?php the_post_thumbnail('matches', array('class' => 'w-100'));?>
                                        </a>
                                    </div>
                                    <div class="col-md-8 col-xs-12">
                                        <a href="<?php the_permalink(); ?>" class="hvr-float-shadow">
                                            <h3 class="text-white text-uppercase font-weight-bold">
                                                <?php the_title(); ?>
                                            </h3>
                                        </a>
                                        <h6 class="text-white text-uppercase">
                                            <strong class="border-right pr-3">
                                                by <?= get_the_author() ?>
                                            </strong>
                                            <strong class="pr-3 pl-3">
                                                <?= get_the_date() ?>
                                            </strong>
                                            <strong class="pl-3">
                                                <?php
                                                    $categories = get_categories();
                                                    foreach(get_the_category() as $category) {
                                                ?>
                                                    <a class="text-white" href="<?= get_category_link($category->term_id) ?>">
                                                        <?= $category->name ?>,
                                                    </a>
                                                <?php } ?>
                                            </strong>
                                        </h6>
                                        <div class="text-white text-justify">
                                            <?php the_excerpt(); ?>
                                        </div>
                                    </div>
                                </div>
                                <hr class="border-white">
                            </div>
                        <?php } wp_reset_postdata(); ?>
                       
                       <div class="col-md-12 text-center mt-3 mb-3">
                           <?php wp_pagenavi(); ?>
                       </div>
                    </div>
                </div>
               
                <div class="col-md-12 mt-5 text-center">
                    <div class="row itemsBanners p-3">
                        <div class="col-md-6 col-xs-12 mb-3">
                            <a href="#" class="hvr-float-shadow">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adds/1.gif" alt="Banners Add" width="100%" height="auto" />
                            </a>
                        </div>
                        <div class="col-md-6 col-xs-12 mb-3">
                            <a href="#" class="hvr-float-shadow">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adds/1.gif" alt="Banners Add" width="100%" height="auto" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <?= get_sidebar(); ?>
        </div>
    </div>
</section>

<?php get_footer();?>