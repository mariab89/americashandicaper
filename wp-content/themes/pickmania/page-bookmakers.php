<?php get_header();?>

<section class="container-fluid mb-5">
    <div class="row">
        <div class="col-md-12 col-xs-12 pl-0 pr-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/slider1.jpg" height="800px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid bookmakers mt-5 mb-5 p-5">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="row mr-2">
                <div class="col-md-12 matchesItems pl-5 pr-5">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <h4 class="bg-light text-center p-3">
                                TOP RATED <br>
                                <strong>
                                    SPORTSBOOKS
                                </strong>
                            </h4>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <p class="float-right">Imga</p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <p class="text-justify text-white">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam, 
                                exercitationem, enim suscipit sint nam quibusdam fugiat odit officia 
                                dolorem facere? Tempora soluta deserunt, odio veniam cumque officia molestiae odit!
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam, 
                                exercitationem, enim suscipit sint nam quibusdam fugiat odit officia 
                                dolorem facere? Tempora soluta deserunt, odio veniam cumque officia molestiae odit!
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam, 
                                exercitationem, enim suscipit sint nam quibusdam fugiat odit officia 
                                dolorem facere? Tempora soluta deserunt, odio veniam cumque officia molestiae odit!
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam, 
                                exercitationem, enim suscipit sint nam quibusdam fugiat odit officia 
                                dolorem facere? Tempora soluta deserunt, odio veniam cumque officia molestiae odit!
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 table-responsive mb-5">
                            <table class="table tableBookMakers table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-white text-center">#</th>
                                        <th class="text-white text-center">SPORTBOOKS</th>
                                        <th class="text-white text-center">DEPOSIT BONUS</th>
                                        <th class="text-white text-center">RATING</th>
                                        <th class="text-white text-center">REVIEW</th>
                                        <th class="text-white text-center">SIGN UP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            <a href="#" class="text-white">
                                                #1
                                            </a>
                                        </th>
                                        <td class="text-center">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="150px" height="auto" />
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                Title
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                A+
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableRead">
                                                READ REVIEW
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableClaim">
                                                CLAIM BONUS
                                            </button>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            <a href="#" class="text-white">
                                                #2
                                            </a>
                                        </th>
                                        <td class="text-center">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="150px" height="auto" />
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                Title
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                A+
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableRead">
                                                READ REVIEW
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableClaim">
                                                CLAIM BONUS
                                            </button>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            <a href="#" class="text-white">
                                                #3
                                            </a>
                                        </th>
                                        <td class="text-center">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="150px" height="auto" />
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                Title
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                A+
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableRead">
                                                READ REVIEW
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableClaim">
                                                CLAIM BONUS
                                            </button>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            <a href="#" class="text-white">
                                                #4
                                            </a>
                                        </th>
                                        <td class="text-center">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="150px" height="auto" />
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                Title
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                A+
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableRead">
                                                READ REVIEW
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableClaim">
                                                CLAIM BONUS
                                            </button>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            <a href="#" class="text-white">
                                                #5
                                            </a>
                                        </th>
                                        <td class="text-center">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="150px" height="auto" />
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                Title
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="text-white">
                                                A+
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableRead">
                                                READ REVIEW
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-block btnSidebarTableClaim">
                                                CLAIM BONUS
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               
                <div class="col-md-12 mt-5 text-center">
                    <div class="row itemsBanners p-3">
                        <div class="col-md-6 col-xs-12 mb-3">
                            <a href="#" class="hvr-float-shadow">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adds/1.gif" alt="Banners Add" width="100%" height="auto" />
                            </a>
                        </div>
                        <div class="col-md-6 col-xs-12 mb-3">
                            <a href="#" class="hvr-float-shadow">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adds/1.gif" alt="Banners Add" width="100%" height="auto" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <?= get_sidebar(); ?>
        </div>
    </div>
</section>

<?php get_footer();?>