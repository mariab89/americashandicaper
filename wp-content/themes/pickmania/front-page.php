<?php get_header();?>

<section class="container-fluid mb-5">
    <div class="row">
        <div class="col-md-12 col-xs-12 pl-0 pr-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/slider1.jpg" height="800px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid mt-5 mb-5 p-5">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="row">
                <div class="col-md-12">
                    <section class="contentPage p-5">
                        <h3>EXPERT SPORT PICKS</h3>
                        <p class="text-justify">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo earum temporibus aspernatur, 
                            exercitationem perferendis repellat. Consequuntur, sunt voluptatum iure labore eum praesentium 
                            quisquam, in culpa, rerum quasi facere perferendis nam!.

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo earum temporibus aspernatur, 
                            exercitationem perferendis repellat. Consequuntur, sunt voluptatum iure labore eum praesentium 
                            quisquam, in culpa, rerum quasi facere perferendis nam!.

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo earum temporibus aspernatur, 
                            exercitationem perferendis repellat. Consequuntur, sunt voluptatum iure labore eum praesentium 
                            quisquam, in culpa, rerum quasi facere perferendis nam!

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo earum temporibus aspernatur, 
                            exercitationem perferendis repellat. Consequuntur, sunt voluptatum iure labore eum praesentium 
                            quisquam, in culpa, rerum quasi facere perferendis nam!

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo earum temporibus aspernatur, 
                            exercitationem perferendis repellat. Consequuntur, sunt voluptatum iure labore eum praesentium 
                            quisquam, in culpa, rerum quasi facere perferendis nam!

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo earum temporibus aspernatur, 
                            exercitationem perferendis repellat. Consequuntur, sunt voluptatum iure labore eum praesentium 
                            quisquam, in culpa, rerum quasi facere perferendis nam!
                        </p>
                    </section>
                </div>
                <div class="col-md-12 mt-5 sectionTopHome">
                    <div class="itemRecord row">
                        <div class="col-md-12">
                            <h4 class="pl-4 text-uppercase">NBA - Click Package to Purchase</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="bg-white border-bottom">
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="itemRecord row">
                        <div class="col-md-12">
                            <h4 class="pl-4 text-uppercase">NCAA-B - Click Package to Purchase</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="bg-white border-bottom">
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="itemRecord row">
                        <div class="col-md-12">
                            <h4 class="pl-4 text-uppercase">NFL - Click Package to Purchase</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="bg-white border-bottom">
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                                <div class="row pl-5 pr-5 pt-3">
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>Art Aronson</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur autem ad.</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3 text-center">
                                        <p>$60.000</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mt-5 text-center">
                    <a href="#" class="hvr-float-shadow">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adds/1.gif" alt="Banners Add" width="100%" height="auto" />
                    </a>
                </div>

                <div class="col-md-12 mt-5 p-5 text-center bg-dark">
                    <a href="#" class="hvr-float-shadow">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adds/1.gif" alt="Banners Add" width="100%" height="auto" />
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <?= get_sidebar(); ?>
        </div>
    </div>
</section>

<?php get_footer();?>