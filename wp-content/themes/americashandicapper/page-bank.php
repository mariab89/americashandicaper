<?php get_header();?>

<section class="container-fluid mt-4 container-section colorBarGeneral borderBottomBank">
    <div class="col-lg-12 col-md-12 text-left textVerticalAlign  noPadding">
        <h2 class="textWhite"> Banking</h2>
    </div>
</section>
<section class="container-fluid container-section">
    <div class="row p-3 separate colorBack">
        <div class="col-lg-12 col-md-12 text-center textVerticalAlign  table-responsive">
            <table class="table bg-transparent tableBanking" width="100%">
                <thead>
                    <tr class="text-uppercase">
                        <th class="itemContentBookThead">Method</th>
                        <th class="itemContentBookThead">FEE</th>
                        <th class="itemContentBookThead">Min*</th>
                        <th class="itemContentBookThead">Max*</th>
                        <th class="itemContentBookThead">Details</th>
                    </tr>
                </thead>
                <tbody>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/1.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/2.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/3.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/4.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/5.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/6.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/7.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/8.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/9.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank ">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/10.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                <tr class="itemContentBank">
                    <td class="logoPay"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/bank/11.png"/></td>
                    <td>Free</td>
                    <td>$10</td>
                    <td>Unlimited</td>
                    <td>free, available depending on your country of residence</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

<?php get_footer();?>