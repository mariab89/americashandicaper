<?php get_header();?>

<section class="container-fluid contentBarHeader container-section-weather">
    <div class="row barMatches m-1">
        <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Weather</span></div>
    </div>
</section>

<section class="container-fluid mt-4 container-section-weather">
    <div class="row mt-4 m-1 imageWeatherTop">
        <div class="col-lg-10 col-md-12">
            <div class="paddingContainerWeather">
                <div class="row">
                    <div class="col-lg-3 col-md-12">
                        <div class="inputOpacityTop">
                            Country
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 columnWeather separate">
                        <div class="inputOpacityTop">
                            State
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="inputOpacityTop">
                            Length
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 columnWeather separate">
                        <div class="inputOpacityTop">
                            Elevation
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="input-group">
                        <div class="col-lg-3 col-md-12">
                            <div class="inputOpacityTop">
                                Time zone
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 columnWeather separate">
                            <div class="inputOpacityTop">
                                City
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12">
                            <div class="inputOpacityTop">
                                Latitude
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 columnWeather separate">
                            <div class="inputOpacityTop">
                                datetime
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
</section>

<section class="container-fluid mt-2 container-section-weather">
    <div class="row mt-4 m-1">
        <div class="col-lg-8 col-md-12 imageWeatherSub">
            <div class="paddingContainerWeather">
                <div class="row">
                    <div class="col-md-12 col-md-12 separate">
                        <div class="inputOpacity">
                            City
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-6 col-md-12">
                        <div class="inputOpacity">
                            State
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 separate">
                        <div class="inputOpacity">
                            Location
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-6 col-md-12">
                        <div class="inputOpacity">
                            Latitude
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 separate">
                        <div class="inputOpacity">
                            Length
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-6 col-md-12">
                        <div class="inputOpacity">
                            Elevation
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 separate">
                        <div class="inputOpacity">
                            Weather
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-6 col-md-12">
                        <div class="inputOpacity">
                            Temperature
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 separate">
                        <div class="inputOpacity">
                            Wind Speed
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-6 col-md-12">
                        <div class="inputOpacity">
                            Direction of the Wind
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 separate">
                        <div class="inputOpacity">
                            Description
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
                <div class="col-md-6">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-6">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
                <div class="col-md-6">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>