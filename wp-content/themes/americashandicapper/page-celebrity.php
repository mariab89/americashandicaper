<?php get_header();?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="#" class="hvr-float-shadow no-image">

            </a>
        </div>
    </div>
</div>


<section class="container-fluid mt-4 container-section">
    <div class="row barMatches">
        <div class="col-lg-12 col-md-12 colorBarGeneral barMatchesContent"><span>Celebrity & history</span></div>
    </div>
</section>

<section class="container-fluid mt-4 container-section">
    <div class="row m-1">
        <div class="col-lg-9 col-md-12 colorContainerRegister separate p-5 textVerticalAlign">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="backInputCelebrityTop textWhite text-uppercase">Text</div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-lg-8 col-md-12">
                    <div class="backInputCelebrityTop textWhite text-uppercase">Thought text</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-12 separate sidebar-list">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
    </div>
</section>

<section class="container-fluid mt-4 container-section">
    <div class="row m-1">
        <div class="col-lg-6 col-md-12">
            <div class="row colorBack p-5">
                <div class="col-md-12 mt-2 text-uppercase textWhite text-center">
                    <h3>today's birthday</h3>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 marginLeft">
            <div class="row colorBack p-5">
                <div class="col-md-12 mt-2 text-uppercase textWhite text-center">
                    <h3>today events</h3>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="backInputCelebrity textWhite text-uppercase"></div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php get_footer();?>