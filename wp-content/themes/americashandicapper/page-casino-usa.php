<?php get_header();?>


<section class="container-fluid mt-4 container-section nivelBar">
    <div class="row barMatches colorBarGeneral p-3">
        <div class="col-lg-1 col-md-12"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/usa.png" width="90" height="50"></div>
        <div class="col-lg-11 col-md-12 textVerticalAlign textWhite"><h1 class="usaTitle">United States</h1></div>
    </div>
</section>

<section class="container-fluid mt-4 container-section">
    <div class="row p-3 separate mt-4 colorBack">
        <div class="col-lg-2 col-md-12 text-center textVerticalAlign">
            <img class="image imageLogoCasino"  src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/2.png">
        </div>
        <div class="col-lg-8 col-md-12 text-white textVerticalAlign">
            <P class="text-justify">Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.</P>
        </div>
        <div class="col-lg-2 col-md-12 textVerticalAlign">
            <a href="#"><img class="buttonCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/1.png"></a>
        </div>
    </div>
    <div class="row p-3 mt-4 colorBack">
        <div class="col-lg-2 col-md-12 text-center textVerticalAlign">
            <img class="image imageLogoCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/3.png">
        </div>
        <div class="col-lg-8 col-md-12 text-white textVerticalAlign">
            <P class="text-justify">Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.</P>
        </div>
        <div class="col-lg-2 col-md-12 textVerticalAlign">
            <a href="#"><img class="buttonCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/1.png"></a>
        </div>
    </div>
    <div class="row p-3 mt-4 colorBack">
        <div class="col-lg-2 col-md-12 text-center textVerticalAlign">
            <img class="image imageLogoCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/4.png">
        </div>
        <div class="col-lg-8 col-md-12 text-white textVerticalAlign">
            <P class="text-justify">Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.</P>
        </div>
        <div class="col-lg-2 col-md-12 textVerticalAlign">
            <a href="#"><img class="buttonCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/1.png"></a>
        </div>
    </div>
    <div class="row p-3 mt-4 colorBack">
        <div class="col-lg-2 col-md-12 text-center textVerticalAlign">
            <img class="image imageLogoCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/5.png">
        </div>
        <div class="col-lg-8 col-md-12 text-white textVerticalAlign">
            <P class="text-justify">Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.</P>
        </div>
        <div class="col-lg-2 col-md-12 textVerticalAlign">
            <a href="#"><img class="buttonCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/1.png"></a>
        </div>
    </div>
    <div class="row p-3 mt-4 colorBack">
        <div class="col-lg-2 col-md-12 text-center textVerticalAlign">
            <img class="image imageLogoCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/6.png">
        </div>
        <div class="col-lg-8 col-md-12 text-white textVerticalAlign">
            <P class="text-justify">Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.</P>
        </div>
        <div class="col-lg-2 col-md-12 textVerticalAlign">
            <a href="#"><img class="buttonCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/1.png"></a>
        </div>
    </div>
    <div class="row p-3 mt-4 colorBack">
        <div class="col-lg-2 col-md-12 text-center textVerticalAlign">
            <img class="image imageLogoCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/7.png">
        </div>
        <div class="col-lg-8 col-md-12 text-white textVerticalAlign">
            <P class="text-justify">Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.</P>
        </div>
        <div class="col-lg-2 col-md-12 textVerticalAlign">
            <a href="#"><img class="buttonCasino" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/usa/1.png"></a>
        </div>
    </div>
</section>

<?php get_footer();?>