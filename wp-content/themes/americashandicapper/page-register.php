<?php get_header();?>

<div class="container mt-5 container-section-login">
    <div class="row">
        <div class="col-lg-12 col-md-12 text-left">
            <a href="#" class="hvr-float-shadow no-image">
            </a>
        </div>
    </div>
</div>

<section class="container-fluid mt-5 container-section-login">
    <div class="row barMatches m-1">
        <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Register</span></div>
    </div>
</section>

<section class="container-fluid colorContainerRegister container-section-login">
    <div class="row mt-4 m-1">
        <div class="col-lg-12 col-md-12">
            <div class="paddingContainerLogin">
                <div class="row">
                    <div class="col-lg-12 col-md-12"><input type="text" class="backInputRegister textWhite text-uppercase" placeholder="Fisrt Name"></div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12 col-md-12"><input type="text" class="backInputRegister textWhite text-uppercase" placeholder="Last Name"></div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12 col-md-12"><input type="text" class="backInputRegister textWhite text-uppercase" placeholder="Country"></div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12 col-md-12"><input type="text" class="backInputRegister textWhite text-uppercase" placeholder="E-mail"></div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12 col-md-12"><input type="text" class="backInputRegister textWhite text-uppercase" placeholder="Password"></div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12 col-md-12"><input type="text" class="backInputRegister textWhite text-uppercase" placeholder="Phone number (optional)"></div>
                </div>
                <div class="row mt-4 textWhite">
                    <div class="col-lg-12 col-md-12 text-justify">
                        <a href="#" class="fontP"><input type="checkbox">&nbsp;I confirm that I am 18 years or older and have read
                            and accept the Terms and Conditions of AMERICASHANDICAPPERS
                            I consent to the processing of my personal data to use this service.</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 centerDiv paddingCard separate">
            <a title="Register" href="#"><img class="imgLogin" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/register.png"/></a>
        </div>
    </div>
</section>

<section class="container-fluid container-section-login">
    <div class="row mt-2">
        <div class="col-lg-3 col-md-12 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
        <div class="col-lg-3 col-md-12 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
        <div class="col-lg-3 col-md-12 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
        <div class="col-lg-3 col-md-12 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
    </div>
</section>

<?php get_footer();?>