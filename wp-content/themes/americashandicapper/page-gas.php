<?php get_header();?>

<section class="container-fluid mt-5 container-section">
    <div class="row barMatches">
        <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Gas</span></div>
    </div>
</section>

<section class="container-fluid container-section">
    <div class="row m-1">
        <div class="col-lg-12 col-md-12 mt-4 noPadding table-responsive">
            <table class="table " width="100%">
                <thead class="tableTop">
                    <tr>
                        <th>Id</th>
                        <th>Regular Price</th>
                        <th>Regular Last Update</th>
                        <th>Premium Price</th>
                        <th>Premium Last Update</th>
                        <th>Diesel Price</th>
                        <th>Diesel Last Update</th>
                    </tr>
                </thead>
                <tbody class="bg-white">
                    <tr class="itemContentBook">
                        <td>1</td>
                        <td>245</td>
                        <td>+22%</td>
                        <td>506</td>
                        <td>121</td>
                        <td>506</td>
                        <td>121</td>
                    </tr>
                    <tr class="itemContentBook">
                        <td>2</td>
                        <td>245</td>
                        <td>+22%</td>
                        <td>506</td>
                        <td>121</td>
                        <td>506</td>
                        <td>121</td>
                    </tr>
                    <tr class="itemContentBook">
                        <td>3</td>
                        <td>245</td>
                        <td>+22%</td>
                        <td>506</td>
                        <td>121</td>
                        <td>506</td>
                        <td>121</td>
                    </tr>
                    <tr class="itemContentBook">
                        <td>4</td>
                        <td>245</td>
                        <td>+22%</td>
                        <td>506</td>
                        <td>121</td>
                        <td>506</td>
                        <td>121</td>
                    </tr>
                </tbody>
            </table>
        </div>
</section>

<?php get_footer();?>