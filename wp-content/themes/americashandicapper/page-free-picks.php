<?php get_header();?>
    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row barMatches">
            <div class="col-lg-12 col-md-12 colorBarGeneral barMatchesContent"><span>Free Picks</span></div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <?= get_sidebar(); ?>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="row p-2">
                    <ul class="nav nav-tabs colorBarGeneral navTabsItemPicks">
                        <li class="active itemTabPicks text-uppercase col-sm-1 text-center"><a data-toggle="tab" href="#homeFree">All</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu1Free">NFL</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu2Free">NBA</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu3Free">MLB</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-2 text-center"><a data-toggle="tab" href="#menu4Free">NCAAB</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu5Free">NHL</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu6Free">CFL</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-2 text-center"><a data-toggle="tab" href="#menu7Free">WNBA</a></li>
                        <li class="active itemTabPicks text-uppercase col-md-2 text-center"><a data-toggle="tab" href="#menu8Free">OTHER</a></li>
                    </ul>
                    <div class="tab-content navTabsItemMatches mt-4">
                        <div id="homeFree" class="tab-pane fade in active show">
                            <div class="itemRecordContent">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorBack text-left barPicks">
                                               &nbsp;
                                            </div>
                                            <div class="row text-uppercase">
                                                <div class="col-lg-6 col-md-12 text-left barPicks">
                                                    <p></p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase"><h2>Arthur</h2></span>
                                                    <span class="text-uppercase"><h2>Jones</h2></span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2">View Analysis</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="itemRecordContent">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorBack text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <p></p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left textVerticalAlignr">
                                                    <span class="text-uppercase"><h2>Arthur</h2></span>
                                                    <span class="text-uppercase"><h2>Jones</h2></span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2">View Analysis</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="itemRecordContent">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorBack text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row itemContent text-uppercase">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <p></p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase"><h2>Arthur</h2></span>
                                                    <span class="text-uppercase"><h2>Jones</h2></span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2">View Analysis</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="itemRecordContent">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorBack text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <p></p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase"><h2>Arthur</h2></span>
                                                    <span class="text-uppercase"><h2>Jones</h2></span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2">View Analysis</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="itemRecordContent">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorBack text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <p></p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase"><h2>Arthur</h2></span>
                                                    <span class="text-uppercase"><h2>Jones</h2></span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2">View Analysis</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <div class="itemRecordContent">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorBack text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <p></p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase"><h2>Arthur</h2></span>
                                                    <span class="text-uppercase"><h2>Jones</h2></span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2">View Analysis</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <div class="itemRecordContent">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorBack text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <p></p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-3 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase"><h2>Arthur</h2></span>
                                                    <span class="text-uppercase"><h2>Jones</h2></span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2">View Analysis</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu1Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 1
                            </div>
                        </div>
                        <div id="menu2Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 2
                            </div>
                        </div>
                        <div id="menu3Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 3
                            </div>
                        </div>
                        <div id="menu4Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 4
                            </div>
                        </div>
                        <div id="menu5Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 5
                            </div>
                        </div>
                        <div id="menu6Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 6
                            </div>
                        </div>
                        <div id="menu7Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 7
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <?php get_sidebar('derecho'); ?>
            </div>
        </div>
    </section>

<?php get_footer();?>