    <?php if (!is_page('bookmakers')){ ?>
        <div class="col-md-12 table-responsive mb-5 sidebar-list">
            <br/>
            <table class="table tableSidebar table-hover table-bordered">
                <caption class="p-2 colorBack itemLine">
                    <h4 class="text-white">TOP BOOKMAKERS</h4>
                </caption>
                <tbody>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #1
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #2
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #3
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #4
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <a href="#">
                                #5
                            </a>
                        </th>
                        <td>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                        </td>
                        <td>
                            <button class="btn btnSidebarTable">
                                PLAY NOW
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 noPadding">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/rain.png">
        </div>
        <div class="col-md-12 mt-4 noPadding">
            <ul class="nav nav-tabs colorTab">
                <li class="active itemTab text-uppercase"><a data-toggle="tab" href="#home">NFL</a></li>
                <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu1">CFB</a></li>
                <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu2">NBA</a></li>
                <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu3">HORSES</a></li>
            </ul>

            <div class="tab-content backTab">
                <div id="home" class="tab-pane fade in active show navTabsItem">
                    <div class="row marginsRowTab">
                            <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <span>BYU VS  WESTERN MICHIGAN</span>
                                <span>PREVIEW AND FREE PICK</span>
                            </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade navTabsItem">
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade navTabsItem">
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade navTabsItem">
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                </div>
                <div id="menu3" class="tab-pane fade navTabsItem">
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade navTabsItem">
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                    <div class="row marginsRowTab">
                        <div class="col-lg-4 col-md-12"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                        <div class="col-lg-8 col-md-12 textVerticalAlign">
                            <span>BYU VS  WESTERN MICHIGAN</span>
                            <span>PREVIEW AND FREE PICK</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-4 noPadding">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
        <div class="col-md-12 mt-4 noPadding">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
    <?php } ?>
