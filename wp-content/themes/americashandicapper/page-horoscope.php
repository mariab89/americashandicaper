<?php get_header();?>

    <section class="container-fluid mt-5 container-section">
        <div class="row barMatches">
            <div class="col-lg-12 col-md-12 colorBarGeneral barMatchesContent"><span>Horoscope</span></div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group input-group colorBarGeneral inputDateHoroscope">
                            <label class="textWhite paddingLabelLeft">Year</label>
                            <div class="col-md-8 paddingCardTop fieldHoroscope">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group input-group colorBarGeneral inputDateHoroscope">
                            <label class="textWhite paddingLabelLeft">Month</label>
                            <div class="col-md-8 paddingCardTop fieldHoroscope">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group input-group colorBarGeneral inputDateHoroscope">
                            <label class="textWhite paddingLabelLeft">Day</label>
                            <div class="col-md-8 paddingCardTop fieldHoroscope">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/12.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Aries</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/10.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Tauro</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope  m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/11.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Geminis</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/9.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Cancer</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/8.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Leo</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope  m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/7.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Virgo</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/6.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Libra</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/5.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Escorpio</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope  m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/4.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Sagitario</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12 contentCenter">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/3.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Capricorinio</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/2.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Acuario</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 noPadding">
                        <div class="row noPadding colorBack paddingTopHoroscope  m-2 mt-4">
                            <div class="col-lg-12 col-md-12 contentCenter">
                                <div class="row noPadding">
                                    <div class="col-lg-3 col-md-3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon-horoscope/1.png"></div>
                                    <div class="col-lg-9 col-md-9 textVerticalAlign"><span class="labelTitleHoroscope text-uppercase textWhite float-right">Piscis</span></div>
                                </div>
                                <div class="row noPadding">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify text-white">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit per, inceptos diam pulvinar nullam mattis aptent hac quis sociis, libero luctus aenean dictumst erat velit rhoncus. Nec cum torquent ornare tincidunt litora malesuada, fringilla dui vitae dignissim inceptos aliquet integer, non cras enim magnis eros. Id pulvinar rhoncus hendrerit mus facilisis eu et leo, purus potenti sed consequat eros semper erat congue bibendum, sodales eleifend proin sociis duis conubia aenean.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="col-md-12">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
                <div class="col-md-12 mt-4">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
                <div class="col-md-12 mt-4">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
                <div class="col-md-12 mt-4">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
            </div>
        </div>
    </section>

<?php get_footer();?>