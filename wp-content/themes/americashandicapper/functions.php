<?php

function theme_styles()
{
    wp_enqueue_style('bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
    wp_enqueue_style('sidebar_css', get_stylesheet_directory_uri() . '/css/sidebar.css');
    wp_enqueue_style('main_css', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style('icon_css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
}

add_action('wp_enqueue_scripts', 'theme_styles');

function theme_js()
{
    global $wp_scripts;
    wp_enqueue_script('jquery_js', 'https://code.jquery.com/jquery-3.3.1.slim.min.js');
    wp_enqueue_script('popper_js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js');
    wp_enqueue_script('bootstrap_js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js');
    wp_enqueue_script('main_js', get_stylesheet_directory_uri() . '/js/main.js');
}

add_action('wp_enqueue_scripts', 'theme_js');

function registrar_sidebar_dereho(){
    register_sidebar(array(
        'name' => 'Sidebar derecho',
        'id' => 'derecho',
        'description' => 'Contenido de sidebar derecho',
        'class' => 'sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}
add_action( 'widgets_init', 'registrar_sidebar_dereho');