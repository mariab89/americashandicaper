<?php get_header();?>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid mt-4 container-section">
        <div class="row barMatches">
            <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Important Matches</span></div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <?= get_sidebar(); ?>
            </div>
            <div class="col-md-6 col-xs-12 p-4 pt-0">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12 sectionTopHomeMatches">
                                <div class="row">
                                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/video.png">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <div class="row">
                                    <ul class="nav nav-tabs colorTab tabMatches navTabsItemMatches">
                                        <li class="active itemTab text-uppercase"><a data-toggle="tab" href="#homeMat">soccer</a></li>
                                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu1Mat">basketball</a></li>
                                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu2Mat">football</a></li>
                                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu3Mat">hockey</a></li>
                                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu4Mat">boxing</a></li>
                                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu5Mat">mma</a></li>
                                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu6Mat">rugby</a></li>
                                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu7Mat">volleyball</a></li>
                                    </ul>
                                    <div class="tab-content bg-white navTabsItemMatches">
                                        <div id="homeMat" class="tab-pane fade in active show">
                                            <div class="row rowMatches1 barMatches">
                                                <div class="col-md-1 textVerticalAlign">
                                                   Date:
                                                </div>
                                                <div class="input-group col-md-4">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text prependLeft" id="basic-addon1">&nbsp;</span>
                                                    </div>
                                                    <input type="date" class="form-control datepickerPage" value="<?php echo date('Y-m-d')?>">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text prependRight" id="basic-addon2">&nbsp;</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Grupo 1 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">Belgian Division 1</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 15 Dec 16:00
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Vittese Arm.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    VVV Venlo
                                                </div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 15 Dec 16:00
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Royal Antwerp.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    W Beveren
                                                </div>
                                            </div>
                                            <!-- Grupo 2 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">German Bundesliga</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 15 Dec 17:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Hannover.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    B Munich
                                                </div>
                                            </div>
                                            <!-- Grupo 3 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">Polish League</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 15 Dec 17:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Slask Wroclaw.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Karona Kielce
                                                </div>
                                            </div>
                                            <!-- Grupo 4 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">Austrian League</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 16 Dec 14:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                   Aus. Vienna.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Rapid Vienna
                                                </div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 16 Dec 14:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    W. Innsbruck.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Wolfsberger
                                                </div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 16 Dec 16:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Aus. Vienna.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Rapid Vienna
                                                </div>
                                            </div>
                                            <!-- Grupo 5 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">Dutch Eredivisie</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 15 Dec 17:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Hannover.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    B Munich
                                                </div>
                                            </div>
                                            <!-- Grupo 6 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">Polish League</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 14 Dec 10:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Slask Wroclaw.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Karona Kielce
                                                </div>
                                            </div>
                                            <!-- Grupo 7 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">Austrian League</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 14 Dec 14:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Aus. Vienna.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Rapid Vienna
                                                </div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 14 Dec 14:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    W. Innsbruck.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Wolfsberger
                                                </div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 14 Dec 16:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Aus. Vienna.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    Rapid Vienna
                                                </div>
                                            </div>
                                            <!-- Grupo 8 -->
                                            <div class="row rowMatches2 barMatches">
                                                <div class="col-md-12">Dutch Eredivisie</div>
                                            </div>
                                            <div class="row itemRowMatches">
                                                <div class="col-xs-12 col-md-3">
                                                    Sa 13 Dec 17:30
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    Hannover.
                                                </div>
                                                <div class="col-xs-12 col-md-2">
                                                    V
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    B Munich
                                                </div>
                                            </div>
                                        </div>
                                        <div id="menu1Mat" class="tab-pane fade">
                                            <div class="alert alert-primary" role="alert">
                                                Content 1
                                            </div>
                                        </div>
                                        <div id="menu2Mat" class="tab-pane fade">
                                            <div class="alert alert-primary" role="alert">
                                                Content 2
                                            </div>
                                        </div>
                                        <div id="menu3Mat" class="tab-pane fade">
                                            <div class="alert alert-primary" role="alert">
                                                Content 3
                                            </div>
                                        </div>
                                        <div id="menu4Mat" class="tab-pane fade">
                                            <div class="alert alert-primary" role="alert">
                                                Content 4
                                            </div>
                                        </div>
                                        <div id="menu5Mat" class="tab-pane fade">
                                            <div class="alert alert-primary" role="alert">
                                                Content 5
                                            </div>
                                        </div>
                                        <div id="menu6Mat" class="tab-pane fade">
                                            <div class="alert alert-primary" role="alert">
                                                Content 6
                                            </div>
                                        </div>
                                        <div id="menu7Mat" class="tab-pane fade">
                                            <div class="alert alert-primary" role="alert">
                                                Content 7
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <?php get_sidebar('derecho'); ?>
            </div>
        </div>
    </section>

<?php get_footer();?>