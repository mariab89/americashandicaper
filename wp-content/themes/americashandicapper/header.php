
<!DOCTYPE html>
<html lang="<?php language_attributes();?>">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo('pingback_url')?>">
	<title><?php wp_title('')?> <?php if (wp_title('', false)) {echo ' : ';}?> <?php bloginfo('name')?></title>
	<!-- IOS -->
	<meta name="apple-mobil-web-app-capable" content="yes">
	<meta name="apple-mobil-web-app-title" content="PREGAMES NOW">
	<link rel="apple-touch-icon" href="<?= get_template_directory_uri() ?>/img/logo.png" >

	<!-- ANDROID -->
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="#262E69">
	<meta name="aplication-name" content="PREGAMES NOW">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri() ?>/img/logo.png" sizes="192x192">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	<link href="<?= get_template_directory_uri() ?>/css/hover-min.css" rel="stylesheet" media="all">
	<?php wp_head();?>
</head>
<body <?php body_class();?>>

<header>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="#" class="hvr-float-shadow no-image">

            </a>
        </div>
    </div>
</div>


<div class="container-fluid containerBar">
    <div class="row">
        <div class="col-lg-12 col-md-12 contentLogin">
            <div class="lineLogin float-right">
                <i class="fas fa-user text-white icon-login"></i>&nbsp;
                <a class="login" href="<?= esc_url(home_url('/login')) ?>">SIGN IN  /</a>
                <a class="login" href="<?= esc_url(home_url('/register')) ?>">REGISTER</a>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 contentHeader">
            <nav class="navbar navbar-expand-lg navbarHeader">
                <a class="navbar-brand animated infinite logo" href="<?= esc_url(home_url('/')) ?>">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" class="logoImage">
                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><i class="fas fa-bars text-white"></i></span>
                </button>

                <div class="collapse navbar-collapse float-right" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item <?php if (is_page('front-page')) echo 'active'; ?>">
                            <a class="nav-link" href="<?= esc_url(home_url('/')) ?>">HOME</a>
                        </li>
                        <li class="nav-item <?php if (is_page('important-matches')) echo 'active'; ?>">
                            <a class="nav-link" href="<?= esc_url(home_url('/important-matches')) ?>">IMPORTANT MATCHES</a>
                        </li>
                        <li class="nav-item <?php if (is_page('top-bookmakers')) echo 'active'; ?>">
                            <a class="nav-link" href="<?= esc_url(home_url('/top-bookmakers')) ?>">TOP BOOKMAKERS</a>
                        </li>
                        <li class="nav-item <?php if (is_page('news')) echo 'active'; ?>">
                            <a class="nav-link" href="<?= esc_url(home_url('/news')) ?>">NEWS</a>
                        </li>
                        <li class="nav-item <?php if (is_page('bookmarkers')) echo 'active'; ?> dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                PICKS
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/free-picks')) ?>">FREE PICKS</a>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/expert-picks')) ?>">EXPERT PICKS</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item <?php if (is_page('bookmarkers')) echo 'active'; ?> dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                SPORTS
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/extras-racing')) ?>">HORSE RACING</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item <?php if (is_page('bookmarkers')) echo 'active'; ?> dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                EXTRAS
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/horoscope')) ?>">HOROSCOPES</a>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/celebrity')) ?>">CELEBRITY & HISTORY</a>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/lottery')) ?>">LOTTERY</a>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/extras-weather')) ?>">WEATHER</a>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/gas')) ?>">GAS</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item <?php if (is_page('casinos')) echo 'active'; ?> dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                CASINOS
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/casino-usa')) ?>">UNITED STATES</a>
                                    <a class="dropdown-item" href="<?= esc_url(home_url('/casino-canada')) ?>">CANADA</a>
                                </div>
                            </div>
                        </li>
                        <li class="search-nav textVerticalAlign float-right">
                            <form class="form-inline">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                            <span id="search">
                                                <i class="fas fa-search"></i>
                                            </span>
                                    </div>
                                    <input type="text" class="form-control inputSearch" aria-describedby="search">
                                    &nbsp;
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
                <hr>
            </nav>
        </div>
    </div>
</div>
</header>

