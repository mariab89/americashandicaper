<?php get_header();?>
    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row barMatches">
            <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Horse Racing</span></div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row p-2">
            <div class="col-lg-9 col-md-12">
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row paddingCard">
                    <div class="col-md-12">
                        <div class="row colorCardNew ">
                            <div class="col-lg-4 col-md-12 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/sports1.png"/></div>
                            <div class="col-lg-8 col-md-12 textVerticalAlign">
                                <h2 class="fontLink">EXPERT SPORTS PICKS:</h2>
                                <p class="marginP fontP">
                                    AMERICAS HANDICAPPER offer you professional picks from a world class
                                    roster built up of 50 OF THE BEST PROFESSIONAL SPORTS HANDICAPPERS
                                    IN THE WORLD,making this your one-stop-shop for finding professional
                                    sports bettors with guaranteed sports picks and predictions to bet on.
                                    Our clients consider us the best sports picks site in the world,
                                    and for a good reason.

                                    The premium PICKS ARE AVAILABLE IMMEDIATELY upon purchase/release,
                                    both in your browser and your email. All Picks are GUARANTEED TO PROFIT
                                    Premium packages are guaranteed with the next day in that sport. If you’ve
                                    purchased a mvv  tttultiple sports package you’ll be guaranteed with an all sports
                                    subscription and long-term subscriptions are guaranteed with an equally
                                    long subscription.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <section class="row">
                    <div class="col-md-12">
                        <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/freepik.png">
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="col-md-12 table-responsive ">
                            <br/>
                            <table class="table table-bordered table-striped allTable" style="background-color: #0a0b0c">
                                <thead class="comingSoonTop">
                                <th class="colorTab  text-uppercase borderBlack">Coming soon</th>
                                </thead>
                                <thead class="worldCup">
                                <th class="text-uppercase borderBlack">World cup<br/><i class="fas fa-angle-down"></i></th>
                                </thead>
                                <tbody class="comingSoon">
                                <tr class="mt-4">
                                    <td class="colorTab comingSoonTop text-uppercase">World cup-qatar 2011 champion</td>
                                </tr>
                                <tr><td class="text-white">Albania</td></tr>
                                <tr><td class="text-white">Algeria</td></tr>
                                <tr><td class="text-white">Argentina</td></tr>
                                <tr><td class="text-white">Australia</td></tr>
                                <tr><td class="text-white">Austria</td></tr>
                                <tr><td class="text-white">Belgium</td></tr>
                                <tr><td class="text-white">Bolivia</td></tr>
                                <tr><td class="text-white">Bosnia - Herzegobina</td></tr>
                                <tr><td class="text-white">Brazil</td></tr>
                                <tr class="mt-4 footerMore">
                                    <td class="colorTab comingSoonTop text-uppercase">See more</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/rain.png">
                    </div>
                    <div class="col-md-12 mt-4">
                        <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                    </div>
                </section>
            </div>
        </div>
    </section>

<?php get_footer();?>