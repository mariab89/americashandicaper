<?php get_header();?>
<section class="container-fluid mt-4 container-section">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid mt-4 container-section">
    <div class="row">
        <div class="col-lg-3 col-md-12">
            <?= get_sidebar(); ?>
        </div>
        <div class="col-lg-6 col-md-12 separate">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/items/item1.jpg">
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-lg-4 col-md-12">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/1.png">
                    <div class="colorBack text-white"><p class="textMargin ">Title</p></div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/2.png">
                    <div class="colorBack text-white"><p class="textMargin">Title</p></div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/3.png">
                    <div class="colorBack text-white"><p class="textMargin">Title</p></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-4 sectionTopHome">
                    <div class="row">
                        <div class="col-md-12 itemRecord">
                            <h4 class="pl-4 text-uppercase">Today's top handicapper picks</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="itemRecordContent">
                                <div class="col-md-12 itemRecordSecond text-left">
                                    <h4 class="p-2 pl-2 text-uppercase">NBA - Click Package to Purchase</h4>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>art aronson</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>aaa's io* nba total "art of war!" +$13,500 all nba ytd!</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>$50.00</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p> doc's sports</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>doc's sports thursday nba top play</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$25.00</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>calvin king</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>thursday premium nba pick - rockets / heat (spread)</p>
                                    </div>
                                    <div class="col-lg-3 col-md-1 text-leftr">
                                        <p>$29.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="itemRecordContent">
                                <div class="col-md-12 itemRecordSecond text-left">
                                    <h4 class="p-2 pl-2 text-uppercase">NCAA-B - CLICK PACKAGE TO PURCHASE</h4>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>jack jones</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>jack's thursday 3-play power pack! (809-604 run up $150,810)</p>
                                    </div>
                                    <div class="col-lg-3 col-md-2 text-leftr">
                                        <p>$59.95</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>jimmy</p>
                                    </div>
                                    <div class="col-lg-6 col-md-7 text-left">
                                        <p>*56% winners l19 days* thursday's 3-pack of profits (1 cbb, 1 cfb, 1nhl)!</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$59.97</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>doc's sports</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>cbb spread on utah state v. houston *104-79 run*!</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$29.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="itemRecordContent">
                                <div class="col-md-12 itemRecordSecond text-left">
                                    <h4 class="p-2 pl-2 text-uppercase">NCAA-F - CLICK PACKAGE TO PURCHASE</h4>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>mike lundin</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>game of the year (CFB): mike's oklahoma vs alabama best bet!</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 col-md-12 text-leftr">
                                        <p>$99.00</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p> marc david</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>david's cfb thursday 10* monster matchup! miami / wisconsin *111-85 run*!</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$60.00</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>jack jones</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>15* marshall/usf gasparilla bowl annihilator! (#1 cfb all-time)</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$34.95</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="itemRecordContent">
                                <div class="col-md-12 itemRecordSecond text-left">
                                    <h4 class="p-2 pl-2 text-uppercase">NFL - CLICK PACKAGE TO PURCHASE</h4>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>mike lundin</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>game of the month (nfl): mike's best nfl total for december test 7-1 run!</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$50.00</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>stephen nover</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>stephen nover's nfl total of the week</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$29.00</p>
                                    </div>
                                </div>
                                <div class="row itemContent text-uppercase">
                                    <div class="col-lg-3 col-md-12 text-left">
                                        <p>jack jones</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 text-left">
                                        <p>15* marshall/usf gasparilla bowl annihilator! (#1 cfb all-time)</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12 text-leftr">
                                        <p>$34.95</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-5 text-center">
                    <a href="#" class="text-uppercase buttonMore">
                        More expert picks
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-12">
            <?php get_sidebar('derecho'); ?>
        </div>
    </div>
</section>

<?php get_footer();?>