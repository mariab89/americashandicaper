<?php get_header();?>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row barMatches">
            <div class="col-lg-12 col-md-12 colorBarGeneral barMatchesContent"><span>News</span></div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-lg-9 col-md-12 p-4 pt-0">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="row colorCardNew ">
                                   <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news1.png"/></div>
                                   <div class="col-md-8 textVerticalAlign">
                                       <h2 class="fontLink">New title super interesting:</h2>
                                       <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                           a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                           IN THE WORLD,making this your one-stop-shop for finding professional
                                           sports bettors with guaranteed sports picks and predictions to bet on.
                                           Our clients consider us the best sports picks site in the world,
                                           and for a good reason.</p>
                                   </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row colorCardNew ">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news2.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <h2 class="fontLink">New title super interesting:</h2>
                                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                            IN THE WORLD,making this your one-stop-shop for finding professional
                                            sports bettors with guaranteed sports picks and predictions to bet on.
                                            Our clients consider us the best sports picks site in the world,
                                            and for a good reason.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row colorCardNew ">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news3.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <h2 class="fontLink">New title super interesting:</h2>
                                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                            IN THE WORLD,making this your one-stop-shop for finding professional
                                            sports bettors with guaranteed sports picks and predictions to bet on.
                                            Our clients consider us the best sports picks site in the world,
                                            and for a good reason.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row colorCardNew ">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news4.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <h2 class="fontLink">New title super interesting:</h2>
                                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                            IN THE WORLD,making this your one-stop-shop for finding professional
                                            sports bettors with guaranteed sports picks and predictions to bet on.
                                            Our clients consider us the best sports picks site in the world,
                                            and for a good reason.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row colorCardNew ">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news5.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <h2 class="fontLink">New title super interesting:</h2>
                                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                            IN THE WORLD,making this your one-stop-shop for finding professional
                                            sports bettors with guaranteed sports picks and predictions to bet on.
                                            Our clients consider us the best sports picks site in the world,
                                            and for a good reason.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row colorCardNew ">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news1.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <h2 class="fontLink">New title super interesting:</h2>
                                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                            IN THE WORLD,making this your one-stop-shop for finding professional
                                            sports bettors with guaranteed sports picks and predictions to bet on.
                                            Our clients consider us the best sports picks site in the world,
                                            and for a good reason.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row colorCardNew ">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news2.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <h2 class="fontLink">New title super interesting:</h2>
                                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                            IN THE WORLD,making this your one-stop-shop for finding professional
                                            sports bettors with guaranteed sports picks and predictions to bet on.
                                            Our clients consider us the best sports picks site in the world,
                                            and for a good reason.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row colorCardNew ">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/news3.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <h2 class="fontLink">New title super interesting:</h2>
                                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                                            IN THE WORLD,making this your one-stop-shop for finding professional
                                            sports bettors with guaranteed sports picks and predictions to bet on.
                                            Our clients consider us the best sports picks site in the world,
                                            and for a good reason.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="col-md-12 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/freepik.png">
                </div>
                <div class="col-md-12 table-responsive mb-4 mt-4">
                    <br/>
                    <table class="table tableSidebar table-hover table-bordered">
                        <caption class="p-2 colorBack itemLine">
                            <h4 class="text-white">TOP BOOKMAKERS</h4>
                        </caption>
                        <tbody>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    #1
                                </a>
                            </th>
                            <td>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                            </td>
                            <td>
                                <button class="btn btnSidebarTable">
                                    PLAY NOW
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    #2
                                </a>
                            </th>
                            <td>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                            </td>
                            <td>
                                <button class="btn btnSidebarTable">
                                    PLAY NOW
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    #3
                                </a>
                            </th>
                            <td>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                            </td>
                            <td>
                                <button class="btn btnSidebarTable">
                                    PLAY NOW
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    #4
                                </a>
                            </th>
                            <td>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                            </td>
                            <td>
                                <button class="btn btnSidebarTable">
                                    PLAY NOW
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <a href="#">
                                    #5
                                </a>
                            </th>
                            <td>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                            </td>
                            <td>
                                <button class="btn btnSidebarTable">
                                    PLAY NOW
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 comingSoon">
                    <div class="table-responsive ">
                        <br/>
                        <table class="table table-bordered table-striped allTable" >
                            <thead class="comingSoonTop">
                                <th class="colorTab  text-uppercase borderBlack">Coming soon</th>
                            </thead>
                            <thead class="worldCup">
                                <th class="text-uppercase borderBlack">World cup<br/><i class="fas fa-angle-down"></i></th>
                            </thead>
                            <tbody class="comingSoon">
                            <tr class="mt-4">
                                <td class="colorTab comingSoonTop text-uppercase">World cup-qatar 2011 champion</td>
                            </tr>
                            <tr><td class="text-white">Albania</td></tr>
                            <tr><td class="text-white">Algeria</td></tr>
                            <tr><td class="text-white">Argentina</td></tr>
                            <tr><td class="text-white">Australia</td></tr>
                            <tr><td class="text-white">Austria</td></tr>
                            <tr><td class="text-white">Belgium</td></tr>
                            <tr><td class="text-white">Bolivia</td></tr>
                            <tr><td class="text-white">Bosnia - Herzegobina</td></tr>
                            <tr><td class="text-white">Brazil</td></tr>
                            <tr class="mt-4 footerMore">
                                <td class="colorTab comingSoonTop text-uppercase">See more</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 mt-4 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
            </div>
        </div>
    </section>

<?php get_footer();?>