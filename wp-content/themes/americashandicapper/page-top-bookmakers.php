<?php get_header();?>
    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row barMatches">
            <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Top Bookmakers</span></div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="col-md-12 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/freepik.png">
                </div>
                <div class="col-md-12 mt-4 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/rain.png">
                </div>
                <div class="col-md-12 mt-4 noPadding">
                    <ul class="nav nav-tabs colorTab">
                        <li class="active itemTab text-uppercase"><a data-toggle="tab" href="#home">NFL</a></li>
                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu1">CFB</a></li>
                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu2">NBA</a></li>
                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu3">HORSES</a></li>
                    </ul>

                    <div class="tab-content backTab">
                        <div id="home" class="tab-pane fade in active show navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                        <div id="menu3" class="tab-pane fade navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 p-4 pt-0">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="row colorBarGeneral ">
                                   <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/image_top1.png"/></div>
                                   <div class="col-md-8 textVerticalAlign">
                                       <p class="marginP fontP">Bovada features the same exceptional wagering options
                                           and creative player propbets and bonuses as sportsbetting
                                           website Bodog,who previously serviced their US based
                                           business. Bovada is a quality online sportsbook with a
                                           great sportsbook and poker room.</p>
                                       <h3 class="fontLink">Go to website</h3>
                                   </div>
                               </div>
                                <div class="row colorBarGeneral mt-2">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/image_top2.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <p class="marginP fontP">Are you a true fan of E-SPORTS?
                                            ODDSWINNERS  features the best options  for e-sports
                                            players,check their quality streaming and bonuses.
                                            ODDSWINNERS is a quality online  with great options
                                            for online gaming.</p>
                                        <h3 class="fontLink">Go to website</h3>
                                    </div>
                                </div>
                                <div class="row colorBarGeneral mt-2">
                                    <div class="col-md-4 noPadding textVerticalAlign"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/image_top3.png"/></div>
                                    <div class="col-md-8 textVerticalAlign">
                                        <p class="marginP fontP">BAVARO features the same exceptional wagering options
                                            and creative player propbets and bonuses as sportsbetting
                                            website Bodog,who previously serviced their US based
                                            business. BAVARO is a quality online sportsbook with
                                            a great sportsbook and poker room.</p>
                                        <h3 class="fontLink">Go to website</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 mt-4 noPadding table-responsive">
                                <table class="table " width="100%">
                                    <thead class="tableTop">
                                    <tr>
                                        <th class="bottonThead">Bookmakers</th>
                                        <th class="bottonThead">Traffic Rank</th>
                                        <th class="bottonThead">Quality</th>
                                        <th class="bottonThead">Ratings</th>
                                        <th class="bottonThead">Comments</th>
                                        <th class="bottonThead">Links</th>
                                    </tr>
                                    </thead>
                                    <tbody class="bg-white">
                                        <tr class="itemContentBook">
                                            <td><img class="tbodyBook" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" /></td>
                                            <td>245</td>
                                            <td>+22%</td>
                                            <td>506</td>
                                            <td>121</td>
                                            <td>
                                                <input class="imageLinkBook" type="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon2.png" />
                                                <input class="imageLinkBook" type="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/icon1.png" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="col-md-12 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
                <div class="col-md-12 mt-4 comingSoon">
                    <div class="col-md-12 table-responsive noPadding">
                        <br/>
                        <table class="table table-bordered table-striped allTable" style="background-color: #0a0b0c">
                            <thead class="comingSoonTop">
                                <th class="colorTab  text-uppercase borderBlack">Coming soon</th>
                            </thead>
                            <thead class="worldCup">
                                <th class="text-uppercase borderBlack">World cup<br/><i class="fas fa-angle-down"></i></th>
                            </thead>
                            <tbody class="comingSoon">
                                <tr class="mt-4">
                                    <td class="colorTab comingSoonTop text-uppercase">World cup-qatar 2011 champion</td>
                                </tr>
                                <tr><td class="text-white">Albania</td></tr>
                                <tr><td class="text-white">Algeria</td></tr>
                                <tr><td class="text-white">Argentina</td></tr>
                                <tr><td class="text-white">Australia</td></tr>
                                <tr><td class="text-white">Austria</td></tr>
                                <tr><td class="text-white">Belgium</td></tr>
                                <tr><td class="text-white">Bolivia</td></tr>
                                <tr><td class="text-white">Bosnia - Herzegobina</td></tr>
                                <tr><td class="text-white">Brazil</td></tr>
                                <tr class="mt-4 footerMore">
                                    <td class="colorTab comingSoonTop text-uppercase">See more</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 mt-4 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
            </div>
        </div>
    </section>

<?php get_footer();?>