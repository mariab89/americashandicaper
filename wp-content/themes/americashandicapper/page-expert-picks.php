<?php get_header();?>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h2 class="titleSlider">TITLE</h2>
                                <button class="btn btnSlider">More</button>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row barMatches">
            <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Expert Picks</span></div>
        </div>
    </section>

    <section class="container-fluid mt-4 container-section">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="col-md-12 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/freepik.png">
                </div>
                <div class="col-md-12 mt-4 noPadding">
                    <div class="col-md-12 table-responsive mb-5 sidebar-list">
                        <br/>
                        <table class="table tableSidebar table-hover table-bordered">
                            <caption class="p-2 colorBack itemLine">
                                <h4 class="text-white">TOP BOOKMAKERS</h4>
                            </caption>
                            <tbody>
                            <tr>
                                <th scope="row">
                                    <a href="#">
                                        #1
                                    </a>
                                </th>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                                </td>
                                <td>
                                    <button class="btn btnSidebarTable">
                                        PLAY NOW
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <a href="#">
                                        #2
                                    </a>
                                </th>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                                </td>
                                <td>
                                    <button class="btn btnSidebarTable">
                                        PLAY NOW
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <a href="#">
                                        #3
                                    </a>
                                </th>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                                </td>
                                <td>
                                    <button class="btn btnSidebarTable">
                                        PLAY NOW
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <a href="#">
                                        #4
                                    </a>
                                </th>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                                </td>
                                <td>
                                    <button class="btn btnSidebarTable">
                                        PLAY NOW
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <a href="#">
                                        #5
                                    </a>
                                </th>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="Banners Add" width="100%" height="auto" />
                                </td>
                                <td>
                                    <button class="btn btnSidebarTable">
                                        PLAY NOW
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 mt-4 noPadding">
                    <ul class="nav nav-tabs colorTab">
                        <li class="active itemTab text-uppercase"><a data-toggle="tab" href="#home">NFL</a></li>
                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu1">CFB</a></li>
                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu2">NBA</a></li>
                        <li class="itemTab text-uppercase"><a data-toggle="tab" href="#menu3">HORSES</a></li>
                    </ul>
                    <div class="tab-content backTab">
                        <div id="home" class="tab-pane fade in active show navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                        <div id="menu3" class="tab-pane fade navTabsItem">
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio1.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio3.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio4.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                            <div class="row marginsRowTab">
                                <div class="col-md-4"><img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/estadio2.jpg"/></div>
                                <div class="col-md-8 textVerticalAlign">
                                    <span>BYU VS  WESTERN MICHIGAN</span>
                                    <span>PREVIEW AND FREE PICK</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 p-4 pt-0">
                <div class="row separador">
                    <div class="col-md-12 noPadding">
                        <div class="row barMatches">
                            <div class="col-md-12 col-xs-12 colorTab barMatchesContent"><span>Expert Picks</span></div>
                        </div>
                    </div>
                </div>
                <div class="row colorCardNew mt-4">
                    <div class="col-md-12 textVerticalAlign p-3">
                        <h2 class="fontLink">New title super interesting:</h2>
                        <p class="marginP fontP">Ccghgth vhgh hnhgfg kaklsd jdnbjeiffe offer you professional picks from
                            a world class roster built up of 50 OF THE BEST PROFESSIONAL SPORTS
                            IN THE WORLD,making this your one-stop-shop for finding professional
                            sports bettors with guaranteed sports picks and predictions to bet on.
                            Our clients consider us the best sports picks site in the world,
                            and for a good reason.</p>
                    </div>
                </div>
                <div class="row mt-2">
                    <ul class="nav nav-tabs colorTab navTabsItemPicks">
                        <li class="active itemTabPicksBlue text-uppercase col-sm-1 text-center"><a data-toggle="tab" href="#homeFree">All</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu1Free">NFL</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu2Free">NBA</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu3Free">MLB</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-2 text-center"><a data-toggle="tab" href="#menu4Free">NCAAB</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu5Free">NHL</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-1 text-center"><a data-toggle="tab" href="#menu6Free">CFL</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-2 text-center"><a data-toggle="tab" href="#menu7Free">WNBA</a></li>
                        <li class="active itemTabPicksBlue text-uppercase col-md-2 text-center"><a data-toggle="tab" href="#menu8Free">OTHER</a></li>
                    </ul>
                    <div class="tab-content navTabsItemMatches mt-4">
                        <div id="homeFree" class="tab-pane fade in active show">
                            <div>
                                <div class="row mt-4">
                                    <div class="col-lg-6 col-md-12 separate">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorTab text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase fontExpertPicks">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-6 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase">Arthur</span>
                                                    <span class="text-uppercase">Jones</span>
                                                    <span class="">$99.00</span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2 mr-4">Buy Now</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 separate">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorTab text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase fontExpertPicks">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-6 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase">Arthur</span>
                                                    <span class="text-uppercase">Jones</span>
                                                    <span class="">$99.00</span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2 mr-4">Buy Now</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-lg-6 col-md-12 separate">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorTab text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase fontExpertPicks">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-6 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase">Arthur</span>
                                                    <span class="text-uppercase">Jones</span>
                                                    <span class="">$99.00</span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2 mr-4">Buy Now</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 separate">
                                        <div class="itemRecordContent">
                                            <div class="col-md-12 colorTab text-left barPicks">
                                                &nbsp;
                                            </div>
                                            <div class="row text-uppercase fontExpertPicks">
                                                <div class="col-lg-6 col-md-12 text-left">
                                                    <img class="image p-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/user.jpg">
                                                </div>
                                                <div class="col-lg-6 col-md-12 text-left textVerticalAlign">
                                                    <span class="text-uppercase">Arthur</span>
                                                    <span class="text-uppercase">Jones</span>
                                                    <span class="">$99.00</span>
                                                    <span class="text-uppercase"><h6 class="colorTab text-center p-2 mr-4">Buy Now</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu1Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 1
                            </div>
                        </div>
                        <div id="menu2Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 2
                            </div>
                        </div>
                        <div id="menu3Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 3
                            </div>
                        </div>
                        <div id="menu4Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 4
                            </div>
                        </div>
                        <div id="menu5Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 5
                            </div>
                        </div>
                        <div id="menu6Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 6
                            </div>
                        </div>
                        <div id="menu7Free" class="tab-pane fade">
                            <div class="alert alert-primary" role="alert">
                                Content 7
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="col-md-12 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
                <div class="col-md-12 mt-4 comingSoon">
                    <div class="col-md-12 table-responsive noPadding">
                        <br/>
                        <table class="table table-bordered table-striped allTable" style="background-color: #0a0b0c">
                            <thead class="comingSoonTop">
                                <th class="colorTab  text-uppercase borderBlack">Coming soon</th>
                            </thead>
                            <thead class="worldCup">
                                <th class="text-uppercase borderBlack">World cup<br/><i class="fas fa-angle-down"></i></th>
                            </thead>
                            <tbody class="comingSoon">
                                <tr class="mt-4">
                                    <td class="colorTab comingSoonTop text-uppercase">World cup-qatar 2011 champion</td>
                                </tr>
                                <tr><td class="text-white">Albania</td></tr>
                                <tr><td class="text-white">Algeria</td></tr>
                                <tr><td class="text-white">Argentina</td></tr>
                                <tr><td class="text-white">Australia</td></tr>
                                <tr><td class="text-white">Austria</td></tr>
                                <tr><td class="text-white">Belgium</td></tr>
                                <tr><td class="text-white">Bolivia</td></tr>
                                <tr><td class="text-white">Bosnia - Herzegobina</td></tr>
                                <tr><td class="text-white">Brazil</td></tr>
                                <tr class="mt-4 footerMore">
                                    <td class="colorTab comingSoonTop text-uppercase">See more</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 mt-4 noPadding">
                    <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
                </div>
            </div>
        </div>
    </section>

<?php get_footer();?>