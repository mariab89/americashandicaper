<section class="row">
    <div class="col-md-12 sidebar-list separate">
        <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/freepik.png">
    </div>
    <div class="col-md-12 mt-4 sidebar-list">
        <div class="col-md-12 table-responsive ">
            <br/>
            <table class="table table-bordered table-striped allTable" style="background-color: #0a0b0c">
                <thead class="comingSoonTop">
                <th class="colorTab  text-uppercase borderBlack">Coming soon</th>
                </thead>
                <thead class="worldCup">
                <th class="text-uppercase borderBlack">World cup<br/><i class="fas fa-angle-down"></i></th>
                </thead>
                <tbody class="comingSoon">
                <tr class="mt-4">
                    <td class="colorTab comingSoonTop text-uppercase">World cup-qatar 2011 champion</td>
                </tr>
                <tr><td class="text-white">Albania</td></tr>
                <tr><td class="text-white">Algeria</td></tr>
                <tr><td class="text-white">Argentina</td></tr>
                <tr><td class="text-white">Australia</td></tr>
                <tr><td class="text-white">Austria</td></tr>
                <tr><td class="text-white">Belgium</td></tr>
                <tr><td class="text-white">Bolivia</td></tr>
                <tr><td class="text-white">Bosnia - Herzegobina</td></tr>
                <tr><td class="text-white">Brazil</td></tr>
                <tr class="mt-4 footerMore">
                    <td class="colorTab comingSoonTop text-uppercase">See more</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 mt-4">
        <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
    </div>
    <div class="col-md-12 mt-4">
        <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
    </div>
</section>