<?php get_header();?>

<section class="container-fluid contentBarHeader container-section-login">
    <div class="row barMatches m-1">
        <div class="col-md-12 col-xs-12 colorBarGeneral barMatchesContent"><span>Sign In</span></div>
    </div>
</section>

<section class="container-fluid colorBack container-section-login">
    <div class="row mt-4 m-1">
        <div class="col-lg-12 col-md-12 separate">
            <div class="paddingContainerLogin">
                <div class="row">
                        <div class="col-lg-12 col-md-12"><input type="text" class="backInputLogin textWhite" placeholder="LOGIN OR EMAIL"></div>
                </div>
                <div class="row mt-4">
                        <div class="col-lg-12 col-md-12"><input type="text" class="backInputLogin textWhite" placeholder="PASSWORD"></div>
                </div>
                <div class="row mt-4 textWhite">
                    <div class="col-lg-6 col-md-12">
                        <input type="checkbox" class="text-uppercase">&nbsp;Remember me</a>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <a href="#" class="text-uppercase textAlign">Forgot Password?</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 centerDiv separate">
            <a title="Sign in" href="#"><img class="imgLogin" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/login.png"/></a>
        </div>
        <div class="col-lg-12 col-md-12 centerDiv mt-4 paddingCard">
            <a title="Google+" href="#"><img class="iconSocial" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/google.png"/></a>
            <a title="Facebook" href="#"><img class="iconSocial" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/facebook.png"/></a>
        </div>
    </div>
</section>

<section class="container-fluid container-section-login">
    <div class="row mt-2">
        <div class="col-lg-3 col-md-6 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
        <div class="col-lg-3 col-md-6 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
        <div class="col-lg-3 col-md-6 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
        <div class="col-lg-3 col-md-6 mt-4">
            <img class="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/elements/space.png">
        </div>
    </div>
</section>

<?php get_footer();?>