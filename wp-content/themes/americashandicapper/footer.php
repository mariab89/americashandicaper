<footer>
    <div class="container-fluid footer1 pl-5 paddingHome">
        <div class="row center-group">
            <div class="col-md-3">
                <h4 class="text-white">CONTACT</h4>
                <ul class="listFooter pl-0">
                    <li>
                        <a href="" class="text-white">
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            Contact
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white">
                           <i class="fa fa-twitter-square"></i>
                        </a>
                        <a href="#" class="text-white">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#" class="text-white">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4 class="text-white">HELP</h4>
                <ul class="listFooter pl-0 te">
                    <li>
                        <a href="" class="text-white">
                            Terms & Conditions
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            Bonus Rules
                        </a>
                    </li>
                    <li>
                        <a href="<?= esc_url(home_url('/bank')) ?>" class="text-white">
                            Banking
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            Privacy Policy
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4 class="text-white">INFORMATION</h4>
                <ul class="listFooter pl-0">
                    <li>
                        <a href="<?= esc_url(home_url('/info')) ?>" class="text-white">
                            About Us
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            Responsible Gaming
                        </a>
                    </li>
                    <li>
                        <a href="" class="text-white">
                            Providers
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="row providerPays">
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/1.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/2.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/3.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/4.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/8.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/5.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/6.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/7.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/9.png" width="150%" class="hvr-pulse-shrink">
            </div>
            <div class="col-6 col-xs-6 col-md-1 mt-5 m-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banks/10.png" width="150%" class="hvr-pulse-shrink">
            </div>
        </div>
    </div>
</footer>
<?php wp_footer()?>

</body>

</html>