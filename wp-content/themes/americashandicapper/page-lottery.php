<?php get_header();?>
<section class="container-fluid mt-4 container-section">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/A.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/B.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/C.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/D.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/E.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/F.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sliders/G.png" height="575px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="titleSlider">TITLE</h2>
                            <button class="btn btnSlider">More</button>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid mt-4 container-section">
    <div class="row barMatches">
        <div class="col-lg-12 col-md-12 colorBarGeneral barMatchesContent"><span>Lottery Result</span></div>
    </div>
</section>

<section class="container-fluid mt-4 container-section">
    <div class="row">
        <div class="col-lg-3 col-md-12">
            <?= get_sidebar(); ?>
        </div>
        <div class="col-md-6 col-xs-12 separate">
            <div class="row colorBack p-3">
                <div class="col-md-6"><div class="backInputCelebrity textWhite text-uppercase">Country</div></div>
                <div class="col-md-6"><div class="backInputCelebrity textWhite text-uppercase">Date</div></div>
            </div>
            <div class="row mt-4 p-3 itemRecordContent">
                <div class="col-lg-12 col-md-12 text-uppercase">
                    <h3>Wed 19 December 2018</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <p>There was one winner of the Lotto Jackpot</p>
                    <p>The winner was sold in the mid west.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-uppercase">
                    WINNING NUMBERS
                    <div class="row m-2">
                        <div class="circleNum circleNumBack">
                            <h2>12</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>6</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>8</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>3</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>14</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 text-uppercase text-center">
                    Bonus
                    <div class="row m-2">
                        <div class="circleNum colorBack">
                            <span class="textWhite">||</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-uppercase">
                   Jacpkot
                    <div class="row m-2">
                        <h1>$300.000</h1>
                    </div>
                </div>
            </div>
            <div class="row mt-4 p-3 itemRecordContent">
                <div class="col-lg-12 col-md-12 text-uppercase">
                    <h3>Wed 19 December 2018</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <p>There was one winner of the Lotto Jackpot</p>
                    <p>The winner was sold in the mid west.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-uppercase">
                    WINNING NUMBERS
                    <div class="row m-2">
                        <div class="circleNum circleNumBack">
                            <h2>12</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>6</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>8</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>3</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>14</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 text-uppercase text-center">
                    Bonus
                    <div class="row m-2">
                        <div class="circleNum colorBack">
                            <span class="textWhite">||</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-uppercase">
                    Jacpkot
                    <div class="row m-2">
                        <h1>$300.000</h1>
                    </div>
                </div>
            </div>
            <div class="row mt-4 p-3 itemRecordContent">
                <div class="col-lg-12 col-md-12 text-uppercase">
                    <h3>Wed 19 December 2018</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <p>There was one winner of the Lotto Jackpot</p>
                    <p>The winner was sold in the mid west.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-uppercase">
                    WINNING NUMBERS
                    <div class="row m-2">
                        <div class="circleNum circleNumBack">
                            <h2>12</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>6</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>8</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>3</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>14</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 text-uppercase text-bonus-align">
                    Bonus
                    <div class="row m-2">
                        <div class="circleNum colorBack">
                            <span class="textWhite">||</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-uppercase">
                    Jacpkot
                    <div class="row m-2">
                        <h1>$300.000</h1>
                    </div>
                </div>
            </div>
            <div class="row mt-4 p-3 itemRecordContent">
                <div class="col-lg-12 col-md-12 text-uppercase">
                    <h3>Wed 19 December 2018</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <p>There was one winner of the Lotto Jackpot</p>
                    <p>The winner was sold in the mid west.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-uppercase">
                    WINNING NUMBERS
                    <div class="row m-2">
                        <div class="circleNum circleNumBack">
                            <h2>12</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>6</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>8</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>3</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>14</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 text-uppercase text-center">
                    Bonus
                    <div class="row m-2">
                        <div class="circleNum colorBack">
                            <span class="textWhite">||</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-uppercase">
                    Jacpkot
                    <div class="row m-2">
                        <h1>$300.000</h1>
                    </div>
                </div>
            </div>
            <div class="row mt-4 p-3 itemRecordContent">
                <div class="col-lg-12 col-md-12 text-uppercase">
                    <h3>Wed 19 December 2018</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <p>There was one winner of the Lotto Jackpot</p>
                    <p>The winner was sold in the mid west.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-uppercase">
                    WINNING NUMBERS
                    <div class="row m-2">
                        <div class="circleNum circleNumBack">
                            <h2>12</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>6</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>8</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>3</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>14</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 text-uppercase text-bonus-align">
                    Bonus
                    <div class="row m-2">
                        <div class="circleNum colorBack">
                            <span class="textWhite">||</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-uppercase">
                    Jacpkot
                    <div class="row m-2">
                        <h1>$300.000</h1>
                    </div>
                </div>
            </div>
            <div class="row mt-4 p-3 itemRecordContent">
                <div class="col-lg-12 col-md-12 text-uppercase">
                    <h3>Wed 19 December 2018</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <p>There was one winner of the Lotto Jackpot</p>
                    <p>The winner was sold in the mid west.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-uppercase">
                    WINNING NUMBERS
                    <div class="row m-2">
                        <div class="circleNum circleNumBack">
                            <h2>12</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>6</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>8</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>3</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>14</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 text-uppercase text-center">
                    Bonus
                    <div class="row m-2">
                        <div class="circleNum colorBack">
                            <span class="textWhite">||</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-uppercase">
                    Jacpkot
                    <div class="row m-2">
                        <h1>$300.000</h1>
                    </div>
                </div>
            </div>
            <div class="row mt-4 p-3 itemRecordContent">
                <div class="col-lg-12 col-md-12 text-uppercase">
                    <h3>Wed 19 December 2018</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <p>There was one winner of the Lotto Jackpot</p>
                    <p>The winner was sold in the mid west.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-uppercase">
                    WINNING NUMBERS
                    <div class="row m-2">
                        <div class="circleNum circleNumBack">
                            <h2>12</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>6</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>8</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>3</h2>
                        </div>
                        <div class="circleNum circleNumBack">
                            <h2>14</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 text-uppercase text-center">
                    Bonus
                    <div class="row m-2">
                        <div class="circleNum colorBack">
                            <span class="textWhite">||</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-uppercase">
                    Jacpkot
                    <div class="row m-2">
                        <h1>$300.000</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-12 separate">
            <?php get_sidebar('derecho'); ?>
        </div>
    </div>
</section>

<?php get_footer();?>