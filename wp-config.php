<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'americashandicapper');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y=jn]!jOyV_: xhc}0zif@;5]w9$^2!SI8BuPhC`1}@T.3wn1hv=Y/P{d7Dsnq/z');
define('SECURE_AUTH_KEY',  '7uT)`[!SI8x<If??Lptafu:qNYtiNg%yI^{L$Orr%t-#N4Zg{n3ajP9|L]L&J^ia');
define('LOGGED_IN_KEY',    '8H,U`k&e@m[&+@z~-1s45tET|_#=v$<W(6`M-c9O/;U 4ig$hwrnC&:PD$Qe|l:T');
define('NONCE_KEY',        'e,,Y6:psY[DOTgqBYBZ_Xx(YO+!=yx7#pAgk2wj?7^tn=`S !$v>`<JDQ1:LSLq`');
define('AUTH_SALT',        '@yRMz/7h]&+g<guPMXq>Wn9,d$K}zf]Xz.^d<`)J=DM)KQ_$Z2Q6A*+^ZVN}OvOg');
define('SECURE_AUTH_SALT', '(-Gmm!S8DXm^uyPylcaCE[B56n`mF)N:]VNPeY^F:FcqJR3W87aW-`Ii[&g(OsjZ');
define('LOGGED_IN_SALT',   'Y,} ?cEO($-Q{BE>^:>?c)IHG3H&M^(8W]PtQXGdDuN!VO%x[hX>r-,jVgx.F`Bm');
define('NONCE_SALT',       'i )3T|H3@X~5Iu<#q@YC-~3P52w BcgImV%S~WWPG?}/lIVH|`n-am*KyK-^RHIK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
